# Silo

Silo is a simple HTTP cache for binaries. Silo does not proxy, but rather allows requests to save and retrieve items from its cache.

This allows easy migration from a filesystem based cache to a (SPOF) remote cache. Making it easier to scale compute nodes of an application that would otherwise be bound to local storage.

It('s SDK) can:

 - Create a cache key
   - From local files (not implemented in API, yet) by both copying and pointing
   - Via API POST
 - Head existence of a cache key
 - Get contents of a cache key (retrieve file)
 - Create download links to share a cached file (without other permissions, read-only)
 - Set TTLs on cache keys
 - Delete cache keys
 - Clean up expired cache keys
 - Protect cache keys!
   - No/invalid token? You'll always get a 403. Unless your request was bad, then you get a 400.
   - No leak of whether a certain key exists or not, unless you have a valid token for that specific key.
   - Single token for each cache key and request method (`HEAD` is also allowed on a `GET`-token for compatibility)
   - Using the SDK you decide how long a download-link is valid
 - Persist cache across restarts
 - Perform graceful restart on `SIGHUP` including cache integrity 

Silo just need 2 things to work
  - Server
     - shared secret for authentication
     - a directory to cache in
  - SDK
     - shared secret for authentication
     - base-URL to the server

For examples on how to use the SDK, you can take a look at `client/client.go`

Wishlist:
  - Cache sync between servers to enable round-robin setups
  - drop least used/oldest cache keys on low disk
  - TLS / HTTPS
  - Update cache key content
  - ETags
  - GoDocs
