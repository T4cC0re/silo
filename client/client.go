package main

import (
	"flag"
	"fmt"
	"github.com/sirupsen/logrus"
	"gitlab.com/T4cC0re/silo/v2/sdk"
	"os"
)

var (
	url      = flag.String("url", "http://localhost:7456", "Base-URL to Silo instance")
	key      = flag.String("key", "", "cache key to retrieve/post/delete (required)")
	secret   = flag.String("secret", "", "secret key to sign request with (required)")
	upload   = flag.String("upload", "", "local file to upload")
	download = flag.String("download", "", "local file to save to")
	deleteK  = flag.Bool("delete", false, "specify to delete the key")
	link     = flag.Bool("link", false, "specify to get a download link")
	ttl      = flag.Int("ttl", 0, "specify with upload to set a TTL on the key")
)

func main() {
	var log = logrus.New()
	flag.Parse()
	if *key == "" || *secret == "" {
		log.Fatalln("You need to set `-key` and `-secret`")
	}

	var err error
	silo := sdk.NewSiloSDKHTTP(*url, *secret, log)

	switch {
	case *upload != "":
		var file *os.File
		file, err = os.Open(*upload)
		if err != nil {
			log.Fatal(err)
		}
		defer file.Close()

		err = silo.Upload(*key, file)
		if err != nil {
			log.Fatalln(err)
		}

		_, err = silo.SetTTL(*key, *ttl)
	case *download != "":
		var file *os.File
		var n int64
		var has bool
		has, err = silo.Has(*key)
		if err != nil {
			log.Fatal(err)
		}
		if !has {
			log.Fatalf("Cache key does not exist")
		}

		file, err = os.Create(*download)
		if err != nil {
			log.Fatal(err)
		}
		defer file.Close()

		n, err = silo.Download(*key, file)
		log.Infof("Downloaded %d bytes to '%s'", n, *download)
	case *deleteK:
		err = silo.Delete(*key)
	case *link:
		log.Infof("Generating download link for '%s' valid for %d seconds", *key, *ttl)
		fmt.Println(silo.GenerateDownloadLink(*key, *ttl))
	case *ttl != 0:
		log.Info("Setting TTL on '%s'", *key)
		ok, err := silo.SetTTL(*key, *ttl)
		if err != nil {
			log.Fatalln(err)
		}
		if !ok {
			os.Exit(1)
		}
	default:
		log.Fatalln("You need to specify an action")
	}

	if err != nil {
		log.Fatal(err)
	}
}
