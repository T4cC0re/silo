module gitlab.com/T4cC0re/silo/v2

go 1.14

require (
	github.com/SermoDigital/jose v0.9.2-0.20180104203859-803625baeddc
	github.com/cloudflare/tableflip v0.0.0-20190201103927-1555bf56e377
	github.com/ghodss/yaml v1.0.0
	github.com/prometheus/client_golang v0.9.2
	github.com/sirupsen/logrus v1.2.0
	gitlab.com/T4cC0re/silo v1.3.0
	gitlab.com/T4cC0re/time-track v0.0.0-20190130002920-f8a364612b66
)
