//+build windows

package silo

import (
	timetrack "gitlab.com/T4cC0re/time-track"
	"os"
	"time"
)

func (fc *FileCache) UsePathAsPointer(key string, path string) (*RWASC, bool, error) {
	defer timetrack.TimeTrack(time.Now())

	if fc.Has(key) {
		return nil, false, os.ErrExist
	}

	stat, err := os.Stat(path)
	if err != nil {
		return nil, false, err
	}

	f, err := os.OpenFile(path, os.O_RDONLY, stat.Mode())
	if err != nil {
		if os.IsPermission(err) {
			return nil, false, os.ErrPermission
		}
		return nil, false, err
	}

	rw := fc.createRawInternal(key, f, path)
	rw.WriteComplete()

	fc.state.Lock()
	fc.isPointer[key] = true
	fc.state.Unlock()
	return rw, true, nil
}
