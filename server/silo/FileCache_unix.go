// +build linux darwin freebsd

package silo

import (
	timetrack "gitlab.com/T4cC0re/time-track"
	"os"
	"syscall"
	"time"
)

func (fc *FileCache) UsePathAsPointer(key string, path string) (*RWASC, bool, error) {
	defer timetrack.TimeTrack(time.Now())

	if fc.Has(key) {
		return nil, false, os.ErrExist
	}

	stat, err := os.Stat(path)
	if err != nil {
		return nil, false, err
	}

	permsOk := false
	if sys, ok := stat.Sys().(*syscall.Stat_t); ok {
		if sys.Uid == uint32(os.Geteuid()) && stat.Mode()&0600 == 0600 {
			// User matches and has rw
			permsOk = true
		}
		if sys.Gid == uint32(os.Getegid()) && stat.Mode()&0060 == 0060 {
			// Group matches and has rw
			permsOk = true
		}
		if stat.Mode()&0006 == 0006 {
			// other has rw
			permsOk = true
		}
	}

	if !permsOk {
		return nil, false, os.ErrPermission
	}

	f, err := os.OpenFile(path, os.O_RDWR, stat.Mode())
	if err != nil {
		return nil, false, err
	}

	rw := fc.createRawInternal(key, f, path)
	rw.WriteComplete()

	fc.state.Lock()
	fc.isPointer[key] = true
	fc.state.Unlock()
	return rw, true, nil
}
