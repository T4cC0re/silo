package silo

import (
	"bufio"
	"encoding/json"
	"gitlab.com/T4cC0re/time-track"
	"io"
	"io/ioutil"
	"os"
	"path"
	"strconv"
	"time"
)

var stateLogTimeout = time.Second * 5

func (fc *FileCache) getLastPid() int {
	defer timetrack.TimeTrack(time.Now())

	file, err := os.Open(path.Join(fc.cacheDir, ".last_pid"))
	if err != nil {
		fc.log.WithField("err", err).Infoln("Failed to get last PID")
		return 0
	}
	data, err := ioutil.ReadAll(file)
	if err != nil {
		fc.log.WithField("err", err).Infoln("Failed to get last PID")
		return 0
	}
	pid, _ := strconv.Atoi(string(data))
	fc.log.WithField("pid", pid).Infof("Got PID")
	return pid
}

func (fc *FileCache) inheritState(pid int) error {
	defer timetrack.TimeTrack(time.Now())
	filepath := path.Join(fc.cacheDir, ".state_"+strconv.Itoa(pid)+".blob")
	file, err := os.Open(filepath)
	if err != nil && !os.IsNotExist(err) {
		return err
	}
	defer file.Close()
	data, err := ioutil.ReadAll(file)
	if err != nil {
		return err
	}
	var state SerializedState
	err = json.Unmarshal(data, &state)
	if err != nil {
		return err
	}

	for _, entry := range state {
		fc.processStateLogEntry(entry)
	}

	fc.log.Infof("State of PID %d loaded", pid)

	return nil
}

func (fc *FileCache) processStateLogEntryRaw(data []byte) (err error) {
	defer timetrack.TimeTrack(time.Now())

	var entry SerializedEntry
	err = json.Unmarshal(data, &entry)
	if err != nil {
		return
	}

	fc.processStateLogEntry(entry)
	return
}

func (fc *FileCache) processStateLogEntry(entry SerializedEntry) {
	defer timetrack.TimeTrack(time.Now())

	fc.state.Lock()
	defer fc.state.Unlock()

	fc.lastLog = time.Now()

	switch entry.Action {
	case ADump, ACreate:
		if entry.CacheKey == "" || entry.FilePath == "" {
			fc.log.Warnf("Invalid entry: %+v", entry)
			return
		}
		fc.cacheMap[entry.CacheKey] = entry.FilePath
		fc.cached[entry.CacheKey] = true
		fc.isPointer[entry.CacheKey] = entry.IsPointer
		fallthrough
	case AModified:
		if entry.CacheKey == "" {
			fc.log.Warnf("Invalid entry: %+v", entry)
			return
		}
		if entry.Expiry > 1 {
			fc.expiry[entry.CacheKey] = time.Unix(entry.Expiry, 0)
		}
	case ARemoved:
		// We have the lock, so we can remove unsafely
		fc.removeUnsafe(entry.CacheKey)

	case ATick:
		// Do nothing
	default:

		fc.log.Warnf("Invalid action %d on entry: %+v", entry.Action, entry)
		return
	}

	fc.log.Infof("Processed entry: %+v", entry)
}

func (fc *FileCache) consumeStateLog(pid int) error {
	file, err := os.Open(path.Join(fc.cacheDir, ".state_"+strconv.Itoa(pid)+".log"))
	if err != nil && !os.IsNotExist(err) {
		return err
	}
	defer file.Close()

	reader := bufio.NewReader(file)
	for {
		line, err := reader.ReadBytes('\n')
		// Nothing read + last log too old == timeout
		if len(line) == 0 && fc.lastLog.Add(stateLogTimeout).Before(time.Now()) {
			fc.log.Infof("State log of PID %d completed (timeout)", pid)
			break
		}
		if err != nil {
			if err == io.EOF {
				if _, err = os.Stat(file.Name()); err != nil {
					// log got deleted or nothing to follow
					fc.log.Infof("State log of PID %d completed (deleted)", pid)
					break
				}
				time.Sleep(time.Millisecond)
				continue
			} else {
				return err
			}
		}
		err = fc.processStateLogEntryRaw(line)
		if err != nil {
			return err
		}

	}

	fc.state.Lock()
	fc.saveState()
	fc.state.Unlock()
	fc.deleteStateFiles(pid)
	return nil
}

// DOES NOT ACQUIRE LOCK! The calling function wishing to log state HAS to have the lock and modified the state before calling this!
func (fc *FileCache) logStateChange(entry SerializedEntry) error {
	defer timetrack.TimeTrack(time.Now())

	if fc.lastSave.IsZero() {
		return fc.saveState()
	}

	serialized, err := json.Marshal(&entry)
	if err != nil {
		return err
	}

	fc.stateLog.Write(serialized)
	fc.stateLog.Write([]byte("\n"))
	fc.stateLog.Sync()
	return nil
}

func (fc *FileCache) startStateLog() error {
	defer timetrack.TimeTrack(time.Now())

	return nil
}

// DOES NOT ACQUIRE LOCK! The calling function wishing to log state HAS to have the lock before calling this!
func (fc *FileCache) saveState() error {
	defer timetrack.TimeTrack(time.Now())

	if fc.lastSave.IsZero() {
		go fc.startStateLog()
	}

	state := SerializedState{}
	for cacheKey, fullyCached := range fc.cached {
		if !fullyCached {
			continue
		}
		entry := SerializedEntry{CacheKey: cacheKey, Action: ADump}
		if fpath := fc.cacheMap[cacheKey]; fpath == "" {
			continue
		} else {
			entry.FilePath = fpath
		}
		if expiry := fc.expiry[cacheKey]; !expiry.IsZero() {
			entry.Expiry = expiry.Unix()
		}
		state = append(state, entry)
	}

	serialized, err := json.Marshal(&state)
	if err != nil {
		return err
	}

	file, err := os.Create(path.Join(fc.cacheDir, ".last_pid"))
	if err != nil {
		return err
	}
	file.WriteString(strconv.Itoa(os.Getpid()))
	file.Sync()
	file.Close()

	filepath := path.Join(fc.cacheDir, ".state_"+strconv.Itoa(os.Getpid())+".blob")
	stateFile, err := os.OpenFile(filepath+"_new", os.O_WRONLY|os.O_CREATE|os.O_TRUNC, 0666)
	if err != nil {
		return err
	}
	defer stateFile.Close()

	stateFile.Write(serialized)
	stateFile.Sync()
	err = os.Rename(filepath+"_new", filepath)
	if err != nil {
		return err
	}
	logname := path.Join(fc.cacheDir, ".state_"+strconv.Itoa(os.Getpid())+".log")
	err = os.Remove(logname)
	if err != nil && !os.IsNotExist(err) {
		return err
	}
	fc.stateLog, err = os.OpenFile(logname, os.O_WRONLY|os.O_CREATE|os.O_APPEND, 0666)
	if err != nil {
		return err
	}
	fc.lastSave = time.Now()
	return nil
}

func (fc *FileCache) deleteStateFiles(pid int) {
	defer timetrack.TimeTrack(time.Now())

	blobname := path.Join(fc.cacheDir, ".state_"+strconv.Itoa(pid)+".blob")
	logname := path.Join(fc.cacheDir, ".state_"+strconv.Itoa(pid)+".log")
	os.Remove(blobname)
	os.Remove(logname)
}
