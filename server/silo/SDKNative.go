package silo

import (
	"fmt"
	silo_sdk "gitlab.com/T4cC0re/silo/v2/sdk"
	timetrack "gitlab.com/T4cC0re/time-track"
	"io"
	"os"
	"strings"
	"time"
)

type SiloSDKNative struct {
	silo    *Silo
	baseURL string
}

func (sdk *SiloSDKNative) finalize() {
	if sdk.baseURL == "" {
		sdk.baseURL = sdk.silo.BaseURL
	}
	sdk.baseURL = strings.TrimRight(sdk.baseURL, "/")
	if !strings.HasPrefix(sdk.baseURL, "/") && !strings.HasPrefix(sdk.baseURL, "http") {
		sdk.baseURL = "/" + sdk.baseURL
	}
}

func (sdk *SiloSDKNative) GenerateDownloadLink(key string, ttl int, baseurl_override ...string) string {
	defer timetrack.TimeTrack(time.Now())

	url := sdk.baseURL
	if len(baseurl_override) > 0 && len(baseurl_override[0]) > 0 {
		url = baseurl_override[0]
	}
	return fmt.Sprintf("%s/%s?access_token=%s", url, key, silo_sdk.SignRequest([]byte(sdk.silo.JwtSecret), "GET", key, ttl))
}

func (sdk *SiloSDKNative) Upload(key string, reader io.Reader) error {
	return sdk.silo.upload(key, 0, reader)
}

func (sdk *SiloSDKNative) Download(key string, writer io.Writer) (int64, error) {
	return sdk.silo.download(key, writer)
}

func (sdk *SiloSDKNative) Delete(key string) error {
	sdk.silo.delete(key)
	return nil
}

func (sdk *SiloSDKNative) Has(key string) (bool, error) {
	return sdk.silo.has(key), nil
}

func (sdk *SiloSDKNative) SetTTL(key string, ttl int) (bool, error) {
	return sdk.silo.fileCache.SetTTL(key, ttl), nil
}

func (sdk *SiloSDKNative) GetRaw(key string) (file *RWASC, err error) {
	if sdk.silo.has(key) {
		file, _, err = sdk.silo.fileCache.Get(key)
		return
	}
	err = os.ErrNotExist
	return
}
