package silo

import (
	"crypto/rand"
	"fmt"
	"github.com/sirupsen/logrus"
	"gitlab.com/T4cC0re/time-track"
	"io"
	"os"
	"path"
	"sync"
	"syscall"
	"time"
)

type RWASC struct {
	file *os.File

	Handle   string
	FC       *FileCache
	CacheKey string

	io.Reader
	io.Writer
	io.ReaderAt
	io.WriterAt
	io.Seeker
	io.Closer
}

const (
	ACreate   = 1
	AModified = 2
	ARemoved  = 3
	ATick     = 128
	ADump     = 255
)

type FileCache struct {
	cacheMap  map[string]string
	cached    map[string]bool
	expiry    map[string]time.Time
	events    map[string]map[string]chan int
	isPointer map[string]bool
	state     *sync.RWMutex // While this could be embedded, this should not be publicly accessible
	stateLog  *os.File
	lastSave  time.Time
	lastLog   time.Time
	cacheDir  string
	log       logrus.FieldLogger
}

type SerializedEntry struct {
	Expiry    int64  `json:"expiry"`
	FilePath  string `json:"path"`
	CacheKey  string `json:"key"`
	IsPointer bool   `json:"is_pointer"`
	Action    uint8  `json:"action"`
}

type SerializedState []SerializedEntry

func NewFileCache(cacheDir string, logger logrus.FieldLogger) *FileCache {
	defer timetrack.TimeTrack(time.Now())
	err := os.MkdirAll(cacheDir, 0755)
	if err != nil {
		panic(err)
	}

	fc := FileCache{
		cacheMap:  map[string]string{},              // Key to filename mapping
		cached:    map[string]bool{},                // Fully cached file?
		expiry:    map[string]time.Time{},           // TTL of the file
		events:    map[string]map[string]chan int{}, // To queue for write finished event/fully cached
		isPointer: map[string]bool{},                // Is a pointer, or uploaded file?
		state:     &sync.RWMutex{},
		stateLog:  nil,
		lastSave:  time.Time{},
		lastLog:   time.Time{},
		cacheDir:  cacheDir,
		log:       logger,
	}

	pid := fc.getLastPid()
	fc.inheritState(pid)
	fc.consumeStateLog(pid)
	fc.saveState()
	go fc.cleanLoop()
	return &fc
}

func (fc *FileCache) cleanLoop() {
	for {
		fc.Clean()
		time.Sleep(time.Second * 15)
	}
}

func (fc *FileCache) Has(key string) bool {
	defer timetrack.TimeTrack(time.Now())
	fc.removeIfExpired(key)
	fc.state.RLock()
	_, exists := fc.cacheMap[key]
	fc.state.RUnlock()

	return exists && fc.cached[key]
}

func (fc *FileCache) removeIfExpired(key string) {
	defer timetrack.TimeTrack(time.Now())

	fc.state.RLock()
	if expiry, hasExpiry := fc.expiry[key]; hasExpiry {
		if !expiry.IsZero() && time.Now().After(expiry) {
			fc.state.RUnlock()
			fc.Remove(key)
			fc.state.RLock()
		}
	}
	fc.state.RUnlock()
}

func (fc *FileCache) Clean() {
	defer timetrack.TimeTrack(time.Now())

	fc.state.RLock()
	for key, ttl := range fc.expiry {
		fc.log.Infof("Key '%s', time: %d, expiry: %d", key, time.Now().Unix(), ttl)
		if ttl.IsZero() {
			fc.log.Infof("Key '%s' is a pointer", key)
			continue
		}
		if time.Now().After(ttl) {
			fc.state.RUnlock()
			fc.Remove(key) // This needs the mutex for a bit
			fc.state.RLock()
			fc.log.Infof("Key '%s' expired", key)
		}
	}
	fc.state.RUnlock()
}

func (fc *FileCache) Remove(key string) {
	defer timetrack.TimeTrack(time.Now())
	fc.state.Lock()
	fc.removeUnsafe(key)
	fc.state.Unlock()
}

// DOES NOT ACQUIRE LOCK! The calling function wishing to log state HAS to have the lock before calling this!
func (fc *FileCache) removeUnsafe(key string) {
	defer timetrack.TimeTrack(time.Now())
	// TODO: Maybe hang on until all open handles are closed

	if file, exists := fc.cacheMap[key]; exists {
		// Skip deletion if it was marked as a pointer
		if !fc.isPointer[key] {
			syscall.Unlink(file)
		}
	}
	delete(fc.cacheMap, key)
	delete(fc.cached, key)
	delete(fc.expiry, key)
	delete(fc.events, key)
	fc.logStateChange(SerializedEntry{Action: ARemoved, CacheKey: key})
}

/*
SetTTL: Set <key> to expire <ttl> seconds from now
*/
func (fc *FileCache) SetTTL(key string, ttl int) bool {
	defer timetrack.TimeTrack(time.Now())

	if !fc.Has(key) {
		return false
	}
	fc.state.Lock()
	if ttl < 0 {
		fc.expiry[key] = time.Time{} // Set to zero time, this will disable expiry
	} else {
		fc.expiry[key] = time.Now().Add(time.Duration(ttl) * time.Second)
	}
	fc.logStateChange(SerializedEntry{Action: AModified, CacheKey: key, Expiry: fc.expiry[key].Unix()})
	fc.state.Unlock()
	return true
}

/*
SetExpiry: Set <key> to expire at <expiry>.
*/
func (fc *FileCache) SetExpiry(key string, expiry time.Time) bool {
	defer timetrack.TimeTrack(time.Now())

	if !fc.Has(key) {
		return false
	}
	fc.state.Lock()
	fc.expiry[key] = expiry
	fc.logStateChange(SerializedEntry{Action: AModified, CacheKey: key, Expiry: expiry.Unix()})
	fc.state.Unlock()
	return true
}

func (fc *FileCache) GetExpiry(key string) time.Time {
	defer timetrack.TimeTrack(time.Now())
	defer fc.state.RUnlock()
	fc.state.RLock()
	return fc.expiry[key]
}

func (fc *FileCache) CopyFromPath(key string, path string) (*RWASC, bool, error) {
	defer timetrack.TimeTrack(time.Now())

	if fc.Has(key) {
		return nil, false, os.ErrExist
	}

	rw, created, err := fc.Get(key)
	if err != nil {
		return nil, false, err
	}
	file, err := os.Open(path)
	if err != nil {
		return nil, false, err
	}
	defer file.Close()
	_, err = io.Copy(rw, file)
	if err != nil {
		return nil, false, err
	}
	rw.WriteComplete()
	return rw, created, nil
}

func (fc *FileCache) Get(key string) (*RWASC, bool, error) {
	defer timetrack.TimeTrack(time.Now())
	var f *os.File
	var err error
	var created bool
	fc.removeIfExpired(key)
	fc.state.Lock()
	if file, exists := fc.cacheMap[key]; exists && fc.cached[key] {
		f, err = os.OpenFile(file, os.O_RDWR, 0666)
		fc.cached[key] = true
		created = false
	} else {
		// We do not log state here, as we only save that of fully cached entries (=write complete)
		f, err = os.Create(path.Join(fc.cacheDir, key))
		fc.cached[key] = false
		created = true
	}
	fc.state.Unlock()
	if err != nil {
		return nil, false, err
	}

	rwasc := fc.createRawInternal(key, f, f.Name())

	return rwasc, created, nil
}

func (fc *FileCache) createRawInternal(key string, file *os.File, filePath string) *RWASC {
	defer timetrack.TimeTrack(time.Now())
	fc.state.Lock()

	fc.cacheMap[key] = filePath
	fc.events[key] = map[string]chan int{}

	fc.state.Unlock()
	buf := make([]byte, 2)
	rand.Read(buf)
	handle := fmt.Sprintf("%x", buf)

	rwasc := &RWASC{
		file:     file,
		Handle:   handle,
		FC:       fc,
		CacheKey: key,
	}
	return rwasc
}

func (f *RWASC) IsWriteComplete() bool {
	defer timetrack.TimeTrack(time.Now())
	defer f.FC.state.RUnlock()
	f.FC.state.RLock()
	return f.FC.cached[f.CacheKey]
}

func (f *RWASC) WriteComplete() {
	defer timetrack.TimeTrack(time.Now())
	f.FC.state.Lock()
	f.FC.cached[f.CacheKey] = true
	f.FC.logStateChange(SerializedEntry{Action: ACreate, IsPointer: f.FC.isPointer[f.CacheKey], CacheKey: f.CacheKey, Expiry: f.FC.expiry[f.CacheKey].Unix(), FilePath: f.FC.cacheMap[f.CacheKey]})
	f.file.Seek(0, 0)
	for _, v := range f.FC.events[f.CacheKey] {
		go func(c chan int) {
			c <- 0
		}(v)
	}
	f.FC.state.Unlock()
}

func (f *RWASC) Close() error {
	defer timetrack.TimeTrack(time.Now())
	defer f.FC.state.Unlock()
	f.FC.state.Lock()
	delete(f.FC.events[f.CacheKey], f.Handle)

	return f.file.Close()
}

func (f *RWASC) Read(p []byte) (n int, err error) {
	f.waitForEOFOnWrite()

	return f.file.Read(p)
}

func (f *RWASC) ReadAt(p []byte, off int64) (n int, err error) {
	f.waitForEOFOnWrite()

	return f.file.ReadAt(p, off)
}

func (f *RWASC) waitForEOFOnWrite() {
	f.FC.state.RLock()
	if !f.FC.cached[f.CacheKey] {
		handle := f.FC.events[f.CacheKey][f.Handle]
		f.FC.state.RUnlock()
		<-handle // Wait for EOF on write
	} else {
		f.FC.state.RUnlock()
	}
}

func (f *RWASC) Seek(offset int64, whence int) (int64, error) {
	defer timetrack.TimeTrack(time.Now())
	if offset == 0 && whence == 0 {
		return f.file.Seek(0, 0)
	}
	f.waitForEOFOnWrite()

	return f.file.Seek(offset, whence)
}

func (f *RWASC) Write(p []byte) (int, error) {
	return f.file.Write(p)
}
func (f *RWASC) WriteAt(p []byte, offset int64) (int, error) {
	return f.file.WriteAt(p, offset)
}
