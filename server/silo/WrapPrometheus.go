package silo

import (
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"net/http"
)

type MetricsWrapper struct {
	Silo *Silo
}

func (wrap *MetricsWrapper) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	if r.URL.Path == "/metrics" {
		promhttp.Handler().ServeHTTP(w, r)
		return
	}

	wrap.Silo.ServeHTTP(w, r)
}
