package silo

import (
	"errors"
	"github.com/SermoDigital/jose/crypto"
	"github.com/SermoDigital/jose/jws"
	"github.com/SermoDigital/jose/jwt"
	"github.com/ghodss/yaml"
	"github.com/sirupsen/logrus"
	"gitlab.com/T4cC0re/silo/v2/sdk"
	"gitlab.com/T4cC0re/time-track"
	"io"
	"io/ioutil"
	"net/http"
	"os"
	"regexp"
	"strconv"
	"strings"
	"time"
)

type Silo struct {
	JwtSecret string `json:"secret,omitempty"`
	CacheDir  string `json:"cache_dir,omitempty"`
	fileCache *FileCache
	sdk       sdk.SiloSDK
	BaseURL   string `json:"base_url,omitempty"`
	log       logrus.FieldLogger
}

var re = regexp.MustCompile(`(?im)^\/([^\/]+)\/([^\/\\\.][^\/\\]*)$`)

func NewSiloFromConfig(config string, logger logrus.FieldLogger) (*Silo, error) {
	defer timetrack.TimeTrack(time.Now())

	file, _ := os.Open(config)
	data, _ := ioutil.ReadAll(file)
	if len(data) == 0 {
		logger.Fatalln("config file empty")
	}

	var silo Silo
	yaml.Unmarshal(data, &silo)
	silo.log = logger

	err := silo.finalize()

	return &silo, err
}

func NewSilo(cacheDir string, jwtSecret string, baseURL string, logger logrus.FieldLogger) (*Silo, error) {
	defer timetrack.TimeTrack(time.Now())
	silo := &Silo{
		JwtSecret: jwtSecret,
		CacheDir:  cacheDir,
		BaseURL:   baseURL,
		log:       logger,
	}
	err := silo.finalize()
	return silo, err
}

func (silo *Silo) finalize() error {
	switch "" {
	case silo.CacheDir, silo.JwtSecret:
		return errors.New("empty config")
	case silo.BaseURL:
		silo.BaseURL = "/silo"
	}

	silo.BaseURL = strings.TrimRight(silo.BaseURL, "/")
	if !strings.HasPrefix(silo.BaseURL, "/") {
		silo.BaseURL = "/" + silo.BaseURL
	}

	if silo.log == nil {
		silo.log = logrus.New()
	}

	silo.fileCache = NewFileCache(silo.CacheDir, silo.log)
	if silo.fileCache == nil {
		return errors.New("fileCache is nil")
	}

	silo.log.
		WithField("cache_dir", silo.CacheDir).
		WithField("base_url", silo.BaseURL).
		Info("Silo instantiated")

	return nil
}

func (silo *Silo) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("server", "Silo")
	w.Header().Set("X-Content-Type-Options", "nosniff")
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Access-Control-Allow-Methods", "GET, HEAD, POST, PUT, DELETE")

	var key string

	// Path is /silo/[key]
	// Where key can contain anything, but slashes and dots
	uri := r.URL.Path

	// Request got mis-routed
	if !strings.HasPrefix(uri, silo.BaseURL) {
		silo.log.Errorf("Ill routed request to '%s'", uri)
		w.WriteHeader(http.StatusNotFound)
		return
	}

	uri = strings.Replace(uri, silo.BaseURL, "", 1)
	splits := strings.Split(uri, "/")
	// Splits[0] would be just ``, because we kept the slash.
	if len(splits) < 2 {
		w.Header().Set("Content-Type", "application/x-silo")
		w.WriteHeader(http.StatusOK)
		_, _ = w.Write([]byte("Silo"))
		return
	} else if len(splits) != 2 {
		silo.log.Error(len(splits), splits)
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	key = splits[1]

	silo.handleContent(w, r, key)
}

func (silo *Silo) handleContent(w http.ResponseWriter, r *http.Request, cacheKey string) {
	cacheKey = strings.ToLower(cacheKey)
	if !silo.hasPermission(r, cacheKey) {
		w.WriteHeader(http.StatusForbidden)
		return
	}

	switch r.Method {
	case "GET", "HEAD":
		if silo.has(cacheKey) {
			// Not using silo.download, because we want to use
			// http.ServeContent
			file, _, err := silo.fileCache.Get(cacheKey)
			if err != nil {
				silo.log.Errorln(err)
				w.WriteHeader(http.StatusInternalServerError)
				return
			}
			defer file.Close()
			http.ServeContent(w, r, cacheKey, silo.fileCache.GetExpiry(cacheKey), file)
			return
		} else {
			w.WriteHeader(http.StatusNotFound)
			return
		}
	case "POST":
		defer r.Body.Close()

		ttl := silo.ttlFromRequest(r)
		err := silo.upload(cacheKey, ttl, r.Body)

		if err == ErrConflict {
			w.WriteHeader(http.StatusConflict)
			return
		} else if err != nil {
			silo.log.Error(err)
			w.WriteHeader(http.StatusInternalServerError)
		}
	case "DELETE":
		silo.delete(cacheKey)
	case "PUT":
		ttl := silo.ttlFromRequest(r)
		if ttl == 0 {
			w.WriteHeader(http.StatusBadRequest)
			return
		}
		if !silo.updateTTL(cacheKey, ttl) {
			w.WriteHeader(http.StatusNotFound)
			return
		}
	default:
		w.WriteHeader(http.StatusMethodNotAllowed)
		return
	}

	w.WriteHeader(200)
}

func (silo *Silo) updateTTL(cacheKey string, ttl int) bool {
	defer timetrack.TimeTrack(time.Now())
	return silo.fileCache.SetTTL(cacheKey, ttl)
}

func (silo *Silo) delete(cacheKey string) {
	defer timetrack.TimeTrack(time.Now())
	silo.fileCache.Remove(cacheKey)
}

func (silo *Silo) has(cacheKey string) bool {
	defer timetrack.TimeTrack(time.Now())
	return silo.fileCache.Has(cacheKey)
}

func (silo *Silo) download(key string, writer io.Writer) (n int64, err error) {
	if !silo.has(key) {
		return 0, os.ErrNotExist
	}

	var file *RWASC
	file, _, err = silo.fileCache.Get(key)
	if err != nil {
		return
	}
	defer file.Close()

	// Get Filesize
	var size int64
	size, err = file.Seek(0, io.SeekEnd)
	if err != nil {
		return
	}
	// rewind
	_, err = file.Seek(0, io.SeekStart)
	if err != nil {
		return
	}

	n, err = io.Copy(writer, file)
	if err != io.EOF {
		return
	}
	if n != size {
		return 0, io.ErrShortWrite
	}

	return n, err
}

var ErrConflict = errors.New("Conflict")

func (silo *Silo) upload(cacheKey string, ttl int, reader io.Reader) error {
	// Do not allow to POST to a existing key
	if silo.fileCache.Has(cacheKey) {
		return ErrConflict
	}

	file, created, err := silo.fileCache.Get(cacheKey)
	if err != nil {
		return err
	}
	defer file.Close()
	if !created {
		return err
	}
	_, err = io.Copy(file, reader)
	if err != nil {
		return err
	}

	// This is important. If this is not called the file is not readable
	file.WriteComplete()

	if ttl != 0 {
		silo.fileCache.SetTTL(cacheKey, ttl)
	}

	return nil
}

func (silo *Silo) ttlFromRequest(r *http.Request) (ttl int) {
	defer timetrack.TimeTrack(time.Now())
	ttl_s := r.Header.Get("silo-ttl")
	if ttl_s != "" {
		ttl, _ = strconv.Atoi(ttl_s)
	}
	return
}

func (silo *Silo) hasPermission(r *http.Request, cacheKey string) bool {
	defer timetrack.TimeTrack(time.Now())
	var token jwt.JWT
	var err error
	var access_token string

	access_token = r.URL.Query().Get("access_token")
	if access_token != "" {
		token, err = jws.ParseJWT([]byte(r.URL.Query().Get("access_token")))
	} else {
		token, err = jws.ParseJWTFromRequest(r)
	}

	if token == nil {
		silo.log.Errorln("token is nil", err)
		return false
	}

	validator := &jwt.Validator{
		EXP: 30 * time.Second,
		NBF: 30 * time.Second,
	}
	if err = token.Validate([]byte(silo.JwtSecret), crypto.SigningMethodHS256, validator); err != nil {
		silo.log.Errorln("token invalid", err)
		return false
	}

	tokenKey := token.Claims().Get("cache_key")
	if val, ok := tokenKey.(string); !ok || !strings.EqualFold(val, cacheKey) {
		silo.log.Errorln("cacheKey invalid", err)
		return false
	}

	tokenMethod, ok := token.Claims().Get("method").(string)
	if !ok {
		silo.log.Errorln("method invalid", err)
		return false
	}

	tokenMethod = strings.ToUpper(tokenMethod)
	method := strings.ToUpper(r.Method)
	switch {
	// Allow if the token method matches the request method, but allow HEAD for GET tokens, too.
	case tokenMethod == "GET" && method == "GET",
		tokenMethod == "GET" && method == "HEAD",
		strings.EqualFold(tokenMethod, method):
		return true
	default:
		silo.log.Errorln("method invalid", err)
		return false
	}
}

func (silo *Silo) Persist() error {
	defer timetrack.TimeTrack(time.Now())
	silo.fileCache.state.RLock()
	defer silo.fileCache.state.RUnlock()
	return silo.fileCache.saveState()
}

/**
EmbeddedSDK: Return a SiloSDK implementation, that does not go over the network
@param baseURL	MUST match the routing to the Silo Handler. This is used for external links
				It MAY be set to a URL including protocol. If unspecified, the baseURL of the
				Silo instance will be inherited
*/
func (silo *Silo) EmbeddedSDK(baseURL string) sdk.SiloSDK {
	if silo.sdk != nil {
		return silo.sdk
	}
	defer timetrack.TimeTrack(time.Now())

	native := &SiloSDKNative{silo: silo, baseURL: baseURL}
	native.finalize()

	silo.sdk = sdk.SiloSDK(native)
	return silo.sdk
}
