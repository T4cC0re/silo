package main

import (
	"context"
	"flag"
	"github.com/cloudflare/tableflip"
	"github.com/sirupsen/logrus"
	s "gitlab.com/T4cC0re/silo/v2/server/silo"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"
)

func main() {
	var level logrus.Level
	var err error
	log := logrus.New().WithField("pid", os.Getpid())
	logfile := flag.String("logfile", "", "Specify to log to file instead of stderr")
	loglevel := flag.String("loglevel", "info", "Specify the desired loglevel (debug, info, warn, error)")
	config := flag.String("config", "", "Path to config file")
	secret := flag.String("secret", "", "JWT secret to use (specify either on commandline or config)")
	base_url := flag.String("base_url", "/silo", "Base URL to listen on")
	cache_dir := flag.String("cache_dir", "", "Cache directory to use (specify either on commandline or config)")
	flag.Parse()

	if *logfile != "" {
		file, err := os.OpenFile(*logfile, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0666)
		if err != nil {
			log.WithField("logfile", *logfile).Fatal(err)
		}
		// Hack Hack
		*os.Stderr = *file
		*os.Stdout = *file
	}
	level, err = logrus.ParseLevel(*loglevel)
	if err != nil {
		panic(err)
	}
	log.Logger.SetLevel(level)

	// Silo boilerplate
	// ----------------------------------------------------
	var silo *s.Silo
	if *config != "" {
		silo, err = s.NewSiloFromConfig(*config, log)
	} else {
		if *secret == "" || *cache_dir == "" {
			flag.PrintDefaults()
			log.WithField("cache_dir", *cache_dir).WithField("secret (len)", len(*secret)).Fatal("Insufficient parameters")
		}

		silo, err = s.NewSilo(*cache_dir, *secret, *base_url, log)
	}

	if err != nil {
		log.Fatalln(err)
	}

	log.Info("Booga")
	log.Println("Booga")

	prom := &s.MetricsWrapper{Silo: silo}
	// ----------------------------------------------------

	// Tableflip boilerplate
	// ----------------------------------------------------
	upg, err := tableflip.New(tableflip.Options{})
	if err != nil {
		panic(err)
	}
	defer upg.Stop()

	go func() {
		stop := make(chan os.Signal, 1)
		hup := make(chan os.Signal, 1)
		signal.Notify(hup, syscall.SIGHUP)
		signal.Notify(stop, os.Interrupt, syscall.SIGTERM)
		for {
			select {
			case <-hup:
				// Persist current Silo state before trying to upgrade
				err := silo.Persist()
				if err != nil {
					log.Errorf("State persistence failed:", err)
					continue
				}
				// Try the actual upgrade
				err = upg.Upgrade()
				if err != nil {
					log.Errorf("Upgrade failed:", err)
					continue
				}

				log.Infof("Upgrade succeeded")
			case <-stop:
				log.Infof("Peristing state...")
				// Persist current Silo state before stopping
				err := silo.Persist()
				if err != nil {
					log.Errorf("State persistence failed:", err)
					os.Exit(1)
				}
				log.Info("State persistence succeeded")
				os.Exit(0)
			}
		}
	}()

	// Get listener (fresh, or from parent)
	ln, err := upg.Fds.Listen("tcp", ":7456")
	if err != nil {
		log.Fatalln("Can't listen:", err)
	}
	log.Info("Listening")
	// ----------------------------------------------------

	// Setup HTTP handler
	var server http.Server
	server.Handler = prom

	// Listen, but only signal ready if there was no error (within the next sleep after this block)
	go func() {
		err := server.Serve(ln)
		if err != nil {
			log.Fatalln(err)
		}
	}()

	// Sleep 500ms to avoid race condition that can yield 'connection reset by peer' errors on clients
	time.Sleep(500 * time.Millisecond)

	// Tell our parent, that we are ready!
	if err := upg.Ready(); err != nil {
		panic(err)
	}

	// We are now officially in business

	// Until this channel is closed, then we should exit
	<-upg.Exit()

	// Failsafe, to quit this process, if the server.Shutdown hangs
	time.AfterFunc(2*time.Minute, func() {
		os.Exit(1)
	})

	// Gracefully drain connections
	_ = server.Shutdown(context.Background())
}
