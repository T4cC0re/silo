package sdk

import (
	"errors"
	"fmt"
	"github.com/SermoDigital/jose/crypto"
	"github.com/SermoDigital/jose/jws"
	"github.com/sirupsen/logrus"
	"gitlab.com/T4cC0re/time-track"
	"io"
	"net/http"
	"strconv"
	"strings"
	"time"
)

type SiloSDKHTTP struct {
	url    string
	secret []byte
	log    logrus.FieldLogger
}

/**
NewSiloSDKHTTP: Returns a SiloSDK implementation via HTTP.
@param url		MUST include the full path to silo routing (`/silo` by default)
@param secret	MUST match with server side. Otherwise requests will fail and links will be invalid.
*/
func NewSiloSDKHTTP(url string, secret string, logger logrus.FieldLogger) (sdk *SiloSDKHTTP) {
	sdk = &SiloSDKHTTP{url, []byte(secret), logger}
	sdk.finalize()
	return
}

func (sdk *SiloSDKHTTP) finalize() {
	if sdk.url == "" {
		panic("URL must not be empty")
	}
	sdk.url = strings.TrimRight(sdk.url, "/")
	if sdk.log == nil {
		sdk.log = logrus.New()
	}
}

func SignRequest(secret []byte, method string, key string, ttl int) string {
	defer timetrack.TimeTrack(time.Now())

	claims := jws.Claims{}
	claims.Set("method", method)
	claims.Set("cache_key", key)
	if ttl != -1 {
		claims.SetExpiration(time.Now().Add(time.Duration(ttl) * time.Second))
	}

	jwt := jws.NewJWT(claims, crypto.SigningMethodHS256)

	b, _ := jwt.Serialize(secret)
	return string(b)
}

func (sdk *SiloSDKHTTP) PerformSignedRequest(method string, key string, reader io.Reader, headers map[string]string) (*http.Response, error) {
	defer timetrack.TimeTrack(time.Now())

	token := SignRequest(sdk.secret, method, key, 10)

	var url string
	if method == "GET" || method == "HEAD" {
		url = fmt.Sprintf("%s/%s?access_token=%s", sdk.url, key, token)
	} else {
		url = fmt.Sprintf("%s/%s", sdk.url, key)
	}

	request, err := http.NewRequest(method, url, reader)
	if err != nil {
		return nil, err
	}
	for header, value := range headers {
		request.Header.Set(header, value)
	}

	if method != "GET" && method != "HEAD" {
		request.Header.Set("Authorization", fmt.Sprintf("Bearer %s", token))
	}

	return http.DefaultClient.Do(request)
}

func (sdk *SiloSDKHTTP) GenerateDownloadLink(key string, ttl int, baseurl_override ...string) string {
	defer timetrack.TimeTrack(time.Now())

	url := sdk.url
	if len(baseurl_override) > 0 && len(baseurl_override[0]) > 0 {
		url = baseurl_override[0]
	}
	return fmt.Sprintf("%s/%s?access_token=%s", url, key, SignRequest(sdk.secret, "GET", key, ttl))
}

func (sdk *SiloSDKHTTP) Upload(key string, reader io.Reader) error {
	defer timetrack.TimeTrack(time.Now())

	response, err := sdk.PerformSignedRequest("POST", key, reader, nil)

	if err != nil {
		return err
	}

	sdk.log.Printf("Headers: %+v", response.Header)

	if response.StatusCode == 200 {
		return nil
	}
	return errors.New(response.Status)
}

func (sdk *SiloSDKHTTP) Download(key string, writer io.Writer) (int64, error) {
	defer timetrack.TimeTrack(time.Now())

	response, err := sdk.PerformSignedRequest("GET", key, nil, nil)

	if err != nil {
		return 0, err
	}

	sdk.log.Printf("Headers: %+v", response.Header)

	if response.StatusCode != 200 {
		return 0, errors.New(response.Status)
	}

	if response.Body == nil {
		return 0, errors.New("no body")
	}
	defer response.Body.Close()

	return io.Copy(writer, response.Body)
}

func (sdk *SiloSDKHTTP) Delete(key string) error {
	defer timetrack.TimeTrack(time.Now())

	response, err := sdk.PerformSignedRequest("DELETE", key, nil, nil)

	if err != nil {
		return err
	}

	sdk.log.Printf("Headers: %+v", response.Header)

	if response.StatusCode == 200 {
		return nil
	}
	return errors.New(response.Status)
}

func (sdk *SiloSDKHTTP) Has(key string) (bool, error) {
	defer timetrack.TimeTrack(time.Now())

	response, err := sdk.PerformSignedRequest("HEAD", key, nil, nil)

	if err != nil {
		return false, err
	}

	sdk.log.Printf("Headers: %+v", response.Header)

	if response.StatusCode == 200 {
		return true, nil
	}
	if response.StatusCode == 404 {
		return false, nil
	}
	return false, errors.New(response.Status)
}

func (sdk *SiloSDKHTTP) SetTTL(key string, ttl int) (bool, error) {
	defer timetrack.TimeTrack(time.Now())

	response, err := sdk.PerformSignedRequest("PUT", key, nil, map[string]string{"silo-ttl": strconv.Itoa(ttl)})

	if err != nil {
		return false, err
	}

	sdk.log.Printf("Headers: %+v", response.Header)

	if response.StatusCode == 200 {
		return true, nil
	}
	if response.StatusCode == 404 {
		return false, nil
	}
	return false, errors.New(response.Status)
}
