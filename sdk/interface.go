package sdk

import (
	"io"
)

type SiloSDK interface {
	GenerateDownloadLink(key string, ttl int, baseurl_override ...string) string
	Upload(key string, reader io.Reader) error
	Download(key string, writer io.Writer) (int64, error)
	Delete(key string) error
	Has(key string) (bool, error)
	SetTTL(key string, ttl int) (bool, error)
}
